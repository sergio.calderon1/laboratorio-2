class Victima < ApplicationRecord

  # Relaciones
  belongs_to :monstruo
  
  # Scopes
  default_scope -> { order(nombre: :asc) }
  scope :mayores, -> { where("edad > 25") }
  
end
