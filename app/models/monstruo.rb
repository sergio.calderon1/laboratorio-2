class Monstruo < ApplicationRecord

  # Validación del nombre (obligatorio y único)
  validates :nombre, presence: true, uniqueness: true
  
  # Relaciones
  # Además al borrar un monstruo también se borran sus víctimas
  has_many :victimas, dependent: :destroy

end
